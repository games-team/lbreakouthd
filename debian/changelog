lbreakouthd (1.1.9-1) unstable; urgency=medium

  * New upstream release.

 -- Stephen Kitt <skitt@debian.org>  Fri, 19 Jul 2024 10:54:44 +0200

lbreakouthd (1.1.8-1) unstable; urgency=medium

  * New upstream release.
  * Constrain the lbreakouthd-data dependency so that the game always
    has the right data.
  * Standards-Version 4.7.0, no change required.

 -- Stephen Kitt <skitt@debian.org>  Wed, 01 May 2024 18:19:08 +0200

lbreakouthd (1.1.7-1) unstable; urgency=medium

  * New upstream release.

 -- Stephen Kitt <skitt@debian.org>  Sun, 07 Apr 2024 14:40:03 +0200

lbreakouthd (1.1.6-1) unstable; urgency=medium

  * New upstream release.

 -- Stephen Kitt <skitt@debian.org>  Sun, 11 Feb 2024 14:56:57 +0100

lbreakouthd (1.1.5-1) unstable; urgency=medium

  * New upstream release.
  * Provide a transitional package to upgrade from LBreakout2.

 -- Stephen Kitt <skitt@debian.org>  Sun, 10 Dec 2023 18:18:07 +0100

lbreakouthd (1.1.4-1) unstable; urgency=medium

  * New upstream release.

 -- Stephen Kitt <skitt@debian.org>  Sun, 01 Oct 2023 15:33:40 +0200

lbreakouthd (1.1.3-1) unstable; urgency=medium

  * New upstream release.
  * Standards-Version 4.6.2, no change required.

 -- Stephen Kitt <skitt@debian.org>  Wed, 14 Jun 2023 18:18:10 +0200

lbreakouthd (1.1.1-1) unstable; urgency=medium

  * New upstream release.

 -- Stephen Kitt <skitt@debian.org>  Sat, 31 Dec 2022 12:30:01 +0100

lbreakouthd (1.1-1) unstable; urgency=medium

  * New upstream release.
  * Standards-Version 4.6.1, no change required.

 -- Stephen Kitt <skitt@debian.org>  Wed, 16 Nov 2022 22:40:45 +0100

lbreakouthd (1.0.10-1) unstable; urgency=medium

  * New upstream release.
  * Allow lbreakouthd-data to satisfy cross-arch dependencies.

 -- Stephen Kitt <skitt@debian.org>  Mon, 02 May 2022 23:31:26 +0200

lbreakouthd (1.0.9-1) unstable; urgency=medium

  * New upstream release.

 -- Stephen Kitt <skitt@debian.org>  Sun, 06 Mar 2022 17:20:45 +0100

lbreakouthd (1.0.8-3) unstable; urgency=medium

  * Split up the arch-dependent and arch-independent build steps so that
    the arch-independent package can be built on its own.
  * Ensure the engine depends on the data package. Closes: #1005651,
    #1005735.
  * Rename the post* files to clarify which package they’re for.

 -- Stephen Kitt <skitt@debian.org>  Mon, 14 Feb 2022 12:11:46 +0100

lbreakouthd (1.0.8-2) unstable; urgency=medium

  * Sourceful upload after NEW.

 -- Stephen Kitt <skitt@debian.org>  Sun, 13 Feb 2022 16:08:03 +0100

lbreakouthd (1.0.8-1) unstable; urgency=medium

  * Initial release. Closes: #1001998.

 -- Stephen Kitt <skitt@debian.org>  Fri, 31 Dec 2021 18:38:08 +0100
